function sweet(icon, title, text) {
  swal({
    title: title,
    icon: icon,
    text: text
  })
}
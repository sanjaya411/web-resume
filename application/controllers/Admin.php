<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    if (!$this->session->userdata("status")) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Masuk!', 'Silahkan login dahulu!')</script>");
			redirect("login");
    }
  }

  public function index()
  {
    $data = [
      "title" => "Dashboard",
      "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row()
    ];
    $this->load->view("templates/header_admin", $data);
    $this->load->view("templates/sidebar_admin", $data);
    $this->load->view("admin/v_index", $data);
    $this->load->view("templates/footer_admin");
  }

  public function website_settings()
  {
    $data = [
      "title" => "Website Settings",
      "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row(),
      "jumbotron" => $this->M_data->getData("jumbotron")->row(),
      "about_me" => $this->M_data->getData("about_me")->row(),
      "skill" => $this->M_data->getData("skill")->result(),
      "sosmed" => $this->M_data->get_sosmed_list()->result(),
      "publish" => $this->M_data->getData("publish")->result(),
      "kategori" => $this->M_data->getData("kategori")->result()
    ];
    $this->load->view("templates/header_admin", $data);
    $this->load->view("templates/sidebar_admin", $data);
    $this->load->view("admin/v_website", $data);
    $this->load->view("templates/footer_admin");
  }

  public function validation_jumbotron()
  {
    $this->form_validation->set_rules('title', 'Jumbotron', 'required');
    $this->form_validation->set_rules('title_jumbotron', 'Jumbotron', 'required');
    $this->form_validation->set_rules('text_jumbotron', 'Jumbotron', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Updated!', 'Isi data dengan benar & lengkap!!')</script>");
			redirect("website_settings");
    } else {
      $this->proses_jumbotron_update();
    }
  }

  private function proses_jumbotron_update()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $gambar = $_FILES["background_gambar"]["name"];

    $this->db->trans_begin();
    $error = false;

    $gambar_sebelumnya = $this->M_data->editData(["jumbotron_id" => 1],"jumbotron")->row();

    $data = [
      "title" => $input->title,
      "title_jumbotron" => $input->title_jumbotron,
      "text_jumbotron" => $input->text_jumbotron
    ];

    $query = $this->M_data->updateData($data,["jumbotron_id" => 1], "jumbotron");
    if ($query) {      
      if ($gambar != "") {
        $gambar_sebelumnya = $this->M_data->editData(["jumbotron_id" => 1],"jumbotron")->row();

        $config['upload_path']          = './assets/img/foto/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 5048;
        $config['max_width']            = 5024;
        $config['max_height']           = 5024;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("background_gambar")) {
          $error = TRUE;
        } else {
          unlink("./assets/img/foto/".$gambar_sebelumnya->background_gambar);
        }
        $data2 = ["background_gambar" => $gambar];
        $this->M_data->updateData($data2, ["jumbotron_id" => 1], "jumbotron");
      }
      if ($error === FALSE) {
        $this->db->trans_commit();
        $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Updated!', 'Data Berhasil diupdate!')</script>");
        redirect("website_settings");
      } else {
        $this->db->trans_rollback();
        $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Updated!', 'Foto gagal diupload!')</script>");
        redirect("website_settings");
      }
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Updated!', 'Query failed!')</script>");
      // var_dump($query);
			redirect("website_settings");
    }
  }

  public function validation_about_me()
  {
    $this->form_validation->set_rules('name', 'About', 'required');
    $this->form_validation->set_rules('text_contact', 'About', 'required');
    $this->form_validation->set_rules('email', 'About', 'required|valid_email');
    $this->form_validation->set_rules('phone', 'About', 'required|numeric');
    $this->form_validation->set_rules('profile', 'About', 'required');
    $this->form_validation->set_rules('alamat', 'About', 'required');
    $this->form_validation->set_rules('about_me', 'About', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Updated!', 'Isi data dengan benar & lengkap!')</script>");
      redirect("website_settings");
    } else {
      $input = (object)$this->input->post();
      $this->proses_about_me_update($input);
    }
  }

  private function proses_about_me_update($input = false)
  {
    $this->db->trans_begin();
    $error = false;
    $data = [
      "name" => html_escape($this->db->escape_str($input->name)),
      "text_contact" => $input->text_contact,
      "email" => html_escape($this->db->escape_str($input->email)),
      "phone" => html_escape($this->db->escape_str($input->phone)),
      "profile" => html_escape($this->db->escape_str($input->profile)),
      "alamat" => html_escape($this->db->escape_str($input->alamat)),
      "about_me" => $input->about_me
    ];
    $check = $this->M_data->updateData($data, ["about_id" => 1], "about_me");
    if ($check) {
      $gambar = $_FILES["gambar"]["name"];

      if($gambar != "") {
        $gambar_sebelumnya = $this->M_data->editData(["about_id" => 1],"about_me")->row();

        $config['upload_path']          = './assets/img/foto/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 5048;
        $config['max_width']            = 5024;
        $config['max_height']           = 5024;

        $this->load->library('upload');
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("gambar")) {
          $error = TRUE;
        } else {
          unlink("./assets/img/foto/".$gambar_sebelumnya->gambar);
        }
        $data2 = ["gambar" => $gambar];
        $this->M_data->updateData($data2, ["about_id" => 1], "about_me");
      }
    }
    if ($error === false) {
      $this->db->trans_commit();
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Updated!', 'Data berhasil diupdate!')</script>");
      redirect("website_settings");
    } else {
      $this->db->trans_rollback();
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Updated!', '".$this->db->error()."')</script>");
      redirect("website_settings");
    }
  }

  public function view_skill_edit($id)
  {
    $skill_id = html_escape($this->db->escape_str($id));
    $check = $this->M_data->editData(["skill_id" => $skill_id], "skill");
    if ($check) {
      $data = [
        "title" => "Website Settings",
        "skill" => $check->row(),
        "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row()
      ];
      $this->load->view("templates/header_admin", $data);
      $this->load->view("templates/sidebar_admin", $data);
      $this->load->view("admin/v_skill_edit", $data);
      $this->load->view("templates/footer_admin");
    }
  }

  public function validation_skill()
  {
    $this->form_validation->set_rules('title', 'fieldlabel', 'required');
    $this->form_validation->set_rules('persentase', 'fieldlabel', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', 'isi data dengan lengkap & benar!')</script>");
      redirect("website_settings");
    } else {
      $input = (object)html_escape($this->db->escape_str($this->input->post()));
      $this->proses_skill_add($input);
    }
  }

  private function proses_skill_add($input = FALSE)
  {
    $data = [
      "title" => $input->title,
      "persentase" => $input->persentase
    ];
    $check = $this->M_data->insertData($data, "skill");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Tambah!', 'Data berhasil ditambah!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', '".$this->db->error()."')</script>");
      redirect("website_settings");
    }
  }

  public function validation_skill_update()
  {
    $this->form_validation->set_rules('title', 'fieldlabel', 'required');
    $this->form_validation->set_rules('persentase', 'fieldlabel', 'required');
    $id = html_escape($this->db->escape_str($this->input->post("skill_id")));

    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', 'isi data dengan lengkap & benar!')</script>");
      redirect("view_skill_edit/".$id);
    } else {
      $input = (object)html_escape($this->db->escape_str($this->input->post()));
      $this->proses_skill_update($input);
    }
  }

  private function proses_skill_update($input = FALSE)
  {
    $data = [
      "title" => $input->title,
      "persentase" => $input->persentase
    ];
    $check = $this->M_data->updateData($data, ["skill_id" => $input->skill_id], "skill");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Tambah!', 'Data berhasil ditambah!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', '".$this->db->error()."')</script>");
      redirect("website_settings");
    }
  }

  public function proses_skill_hapus($id)
  {
    $skill_id = html_escape($this->db->escape_str($id));
    $check = $this->M_data->deleteData(["skill_id" => $skill_id], "skill");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Hapus!', 'Data berhasil dihapus!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Update!', '".$this->db->error()."')</script>");
      redirect("website_settings");
    }
  }

  public function validation_sosmed()
  {
    $this->form_validation->set_rules('nama', 'Sosmed', 'required');
    $this->form_validation->set_rules('ikon', 'Sosmed', 'required');
    $this->form_validation->set_rules('link', 'Sosmed', 'required');
    $this->form_validation->set_rules('publish_id', 'Sosmed', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Isi data dengan lengkap & benar!')</script>");
      redirect("website_settings");
    } else {
      $this->proses_sosmed_tambah();
    }
  }

  private function proses_sosmed_tambah()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "nama" => $input->nama,
      "ikon" => $input->ikon,
      "link" => $input->link,
      "publish_id" => $input->publish_id
    ];
    $check = $this->M_data->insertData($data, "sosmed");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data berhasil ditambahkan!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("website_settings");
    }
  }

  public function sosmed_edit($id)
  {
    $sosmed_id = (int)$this->db->escape_str($id);
    $check = $this->M_data->editData(["sosmed_id" => $sosmed_id], "sosmed");
    if ($check) {
      $data = [
        "title" => "Website Settings",
        "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row(),
        "sosmed" => $check->row(),
        "publish" => $this->M_data->getData("publish")->result()
      ];
      $this->load->view("templates/header_admin", $data);
      $this->load->view("templates/sidebar_admin", $data);
      $this->load->view("admin/v_sosmed_edit", $data);
      $this->load->view("templates/footer_admin");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("website_settings");
    }
  }

  public function validation_sosmed_edit()
  {
    $this->form_validation->set_rules('nama', 'Sosmed', 'required');
    $this->form_validation->set_rules('ikon', 'Sosmed', 'required');
    $this->form_validation->set_rules('link', 'Sosmed', 'required');
    $this->form_validation->set_rules('publish_id', 'Sosmed', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Isi data dengan lengkap & benar!')</script>");
      $id = html_escape($this->input->post("sosmed_id"));
      redirect("sosmed_edit/".$id);
    } else {
      $this->proses_sodmed_update();
    }
  }

  private function proses_sodmed_update()
  {
    $input = (object)html_escape($this->db->escape_str($this->input->post()));
    $data = [
      "nama" => $input->nama,
      "ikon" => $input->ikon,
      "link" => $input->link,
      "publish_id" => $input->publish_id
    ];
    $check = $this->M_data->updateData($data, ["sosmed_id" => $input->sosmed_id], "sosmed");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data berhasil diupdate!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("sosmed_edit/".$input->sosmed_id);
    }
  }

  public function sosmed_hapus($sosmed_id)
  {
    $id = (int)$this->db->escape_str($sosmed_id);
    $check = $this->M_data->deleteData(["sosmed_id" => $id], "sosmed");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data berhasil dihapus!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("website_settings");
    }
  }

  public function proses_kategori_tambah()
  {
    $input = html_escape($this->db->escape_str($this->input->post("kategori")));
    $data = ["kategori" => $input];
    $check = $this->M_data->insertData($data, "kategori");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data berhasil ditambah!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Query failed!')</script>");
      redirect("website_settings");
    }
  }

  public function proses_kategori_hapus($kategori_id)
  {
    $id = (int)$kategori_id;
    $check = $this->M_data->deleteData(["kategori_id" => $id], "kategori");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data berhasil dihapus!')</script>");
      redirect("website_settings");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("website_settings");
    }
  }

  public function portofolio()
  {
    $data = [
      "title" => "Portofolio",
      "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row(),
      "portofolio" => $this->M_data->get_portofolio_kategori()->result(),
      "kategori" => $this->M_data->getData("kategori")->result()
    ];
    $this->load->view("templates/header_admin", $data);
    $this->load->view("templates/sidebar_admin", $data);
    $this->load->view("admin/v_portofolio", $data);
    $this->load->view("templates/footer_admin");
  }

  public function validation_portofolio_tambah()
  {
    $this->form_validation->set_rules('judul', 'fieldlabel', 'required');
    $this->form_validation->set_rules('kategori_id', 'fieldlabel', 'required');
    $this->form_validation->set_rules('keterangan', 'fieldlabel', 'required');
    $this->form_validation->set_rules('tanggal_buat', 'fieldlabel', 'required');
    $this->form_validation->set_rules('publish_id', 'fieldlabel', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', 'Isi data dengan benar &lengkap!')</script>");
      redirect("portofolio");
    } else {
      $this->proses_portofolio_tambah();
    }    
  }

  private function proses_portofolio_tambah()
  {
    $error = FALSE;
    $input = (object)$this->input->post();
    $this->db->trans_begin();
    $data = [
      "judul" => html_escape($this->db->escape_str($input->judul)),
      "kategori_id" => html_escape($this->db->escape_str($input->kategori_id)),
      "tanggal_buat" => $input->tanggal_buat,
      "tanggal_upload" => date("Y-m-d"),
      "tanggal_update" => date("Y-m-d"),
      "keterangan" => $input->keterangan,
      "publish_id" => $input->publish_id
    ];
    $check = $this->M_data->insertData($data, "portofolio");
    $portofolio_id = $this->db->insert_id();
    if ($check) {
      $gambar = $_FILES["gambar"]["name"];
      if ($gambar != "") {

        $config['upload_path']          = './assets/img/portofolio/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 5048;
        $config['max_width']            = 5024;
        $config['max_height']           = 5024;

        $this->load->library("upload");
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("gambar")) {
          $error = TRUE;
        } else {
          $data = ["gambar" => $gambar];
          $check = $this->M_data->updateData($data, ["portofolio_id" => $portofolio_id], "portofolio");
          if (!$check) {
            $error = TRUE;
          }
        }
      }
      if ($error === FALSE) {
        $this->db->trans_commit();
        $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Tambah!', 'Data telah ditambah!')</script>");
        redirect("portofolio");
      } else {
        $this->db->trans_rollback();
        $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', 'Gambar gagal upload!')</script>");
        redirect("portofolio");
      }
    }
  }

  public function view_portofolio_edit($data = FALSE)
  {
    $id = (int)$this->db->escape_str($data);
    $check = $this->M_data->editData(["portofolio_id" => $id], "portofolio");
    if ($check) {
      $data = [
        "title" => "Portofolio",
        "portofolio" => $check->row(),
        "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row(),
        "kategori" => $this->M_data->getData("kategori")->result()
      ];
      $this->load->view("templates/header_admin", $data);
      $this->load->view("templates/sidebar_admin", $data);
      $this->load->view("admin/v_portofolio_edit", $data);
      $this->load->view("templates/footer_admin");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("portofolio");
    }
  }

  public function validation_portofolio_edit()
  {
    $this->form_validation->set_rules('judul', 'fieldlabel', 'required');
    $this->form_validation->set_rules('kategori_id', 'fieldlabel', 'required');
    $this->form_validation->set_rules('keterangan', 'fieldlabel', 'required');
    $this->form_validation->set_rules('tanggal_buat', 'fieldlabel', 'required');
    $this->form_validation->set_rules('publish_id', 'fieldlabel', 'required');
    $id = (int)$this->db->escape_str($this->input->post("portofolio_id"));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Update!', 'Isi data dengan benar &lengkap!')</script>");
      redirect("edit_portofolio/".$id);
    } else {
      $this->proses_portofolio_update();
    }
  }

  private function proses_portofolio_update()
  {
    $error = FALSE;
    $input = (object)$this->input->post();
    $this->db->trans_begin();
    $data = [
      "judul" => html_escape($this->db->escape_str($input->judul)),
      "kategori_id" => html_escape($this->db->escape_str($input->kategori_id)),
      "tanggal_buat" => $input->tanggal_buat,
      "tanggal_update" => date("Y-m-d"),
      "keterangan" => $input->keterangan,
      "publish_id" => $input->publish_id
    ];
    $check = $this->M_data->updateData($data, ["portofolio_id" => $input->portofolio_id], "portofolio");
    if ($check) {
      $gambar = $_FILES["gambar"]["name"];
      if ($gambar != "") {
        $gambar_sebelumnya = $this->M_data->editData(["portofolio_id" => $input->portofolio_id], "portofolio")->row();

        $config['upload_path']          = './assets/img/portofolio/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 5048;
        $config['max_width']            = 5024;
        $config['max_height']           = 5024;

        $this->load->library("upload");
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("gambar")) {
          $error = TRUE;
        } else {
          unlink("./assets/img/portofolio/".$gambar_sebelumnya->gambar);
          $data = ["gambar" => $gambar];
          $check = $this->M_data->updateData($data, ["portofolio_id" => $input->portofolio_id], "portofolio");
          if (!$check) {
            $error = TRUE;
          }
        }
      }
      if ($error === FALSE) {
        $this->db->trans_commit();
        $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses Tambah!', 'Data telah ditambah!')</script>");
        redirect("portofolio");
      } else {
        $this->db->trans_rollback();
        $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Tambah!', 'Gambar gagal upload!')</script>");
        redirect("edit_portofolio/".$input->portofolio_id);
      }
    }
  }

  public function proses_portofolio_hapus($data = FALSE)
  {
    $id = (int)$this->db->escape_str($data);
    $check = $this->M_data->editData(["portofolio_id" => $id], "portofolio");
    if ($check) {
      unlink("./assets/img/portofolio/".$check->row()->gambar);
      $check2 = $this->M_data->deleteData(["portofolio_id" => $id], "portofolio");
      if ($check2) {
        $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data telah dihapus!')</script>");
        redirect("portofolio");
      } else {
        $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
        redirect("portofolio");
      }
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Data tidak ditemukan!')</script>");
      redirect("portofolio");
    }
  }

  public function view_blog()
  {
    $data = [
      "title" => "Blog",
      "blog" => $this->M_data->get_blog_kategori()->result(),
      "kategori" => $this->M_data->getData("kategori")->result(),
      "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row()
    ];
    $this->load->view("templates/header_admin", $data);
    $this->load->view("templates/sidebar_admin", $data);
    $this->load->view("admin/v_blog", $data);
    $this->load->view("templates/footer_admin");
  }

  public function validation_blog()
  {
    $this->form_validation->set_rules('judul', 'fieldlabel', 'required');
    $this->form_validation->set_rules('kategori_id', 'fieldlabel', 'required');
    $this->form_validation->set_rules('article', 'fieldlabel', 'required');
    $this->form_validation->set_rules('publish_id', 'fieldlabel', 'required');
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Isi data dengan benar & lengkap!')</script>");
        redirect("blog");
    } else {
      $this->proses_blog_tambah();
    }
  }

  private function proses_blog_tambah()
  {
    $error = FALSE;
    $pesan = "";
    $input = (object)$this->input->post();
    $this->db->trans_begin();

    $data = [
      "kategori_id" => html_escape($this->db->escape_str($input->kategori_id)),
      "publish_id" => html_escape($this->db->escape_str($input->publish_id)),
      "judul" => html_escape($this->db->escape_str($input->judul)),
      "penulis" => $this->session->userdata("admin_id"),
      "tanggal_buat" => date("Y-m-d"),
      "tanggal_update" => date("Y-m-d"),
      "article_pendek" => $input->article_pendek,
      "article" => $input->article
    ];

    $check = $this->M_data->insertData($data, "blog");
    $blog_id = $this->db->insert_id();
    if ($check) {
      $gambar = $_FILES["gambar"]["name"];
      if ($gambar != "") {

        $config['upload_path']          = './assets/img/blog/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 5048;
        $config['max_width']            = 5024;
        $config['max_height']           = 5024;

        $this->load->library("upload");
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("gambar")) {
          $error = TRUE;
          $pesan = "Gambar gagal Upload!";
        } else {
          $data = ["gambar" => $gambar];
          $check2 = $this->M_data->updateData($data, ["blog_id" => $blog_id], "blog");
          if (!$check2) {
            $error = TRUE;
            $pesan = "Query2 Failed";
          }
        }
      }
    } else {
      $error = TRUE;
      $pesan = "Query1 Failed";
    }
    if ($error === FALSE) {
      $this->db->trans_commit();
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Artikel berhasil ditambah!')</script>");
      redirect("blog");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', '".$pesan."')</script>");
        redirect("blog");
    }
  }

  public function view_blog_edit($data = FALSE)
  {
    $id = (int)$this->db->escape_str($data);
    $check = $this->M_data->editData(["blog_id" => $id], "blog");
    if ($check) {
      $data = [
        "title" => "Blog",
        "kategori" => $this->M_data->getData("kategori")->result(),
        "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row(),
        "blog" => $check->row()
      ];
      $this->load->view("templates/header_admin", $data);
      $this->load->view("templates/sidebar_admin", $data);
      $this->load->view("admin/v_blog_edit", $data);
      $this->load->view("templates/footer_admin");
    }
  }

  public function validation_blog_edit()
  {
    $this->form_validation->set_rules('judul', 'fieldlabel', 'required');
    $this->form_validation->set_rules('kategori_id', 'fieldlabel', 'required');
    $this->form_validation->set_rules('article', 'fieldlabel', 'required');
    $this->form_validation->set_rules('publish_id', 'fieldlabel', 'required');
    $id = (int)$this->db->escape_str($this->input->post("blog_id"));
    
    if ($this->form_validation->run() == FALSE) {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Isi data dengan benar & lengkap!')</script>");
        redirect("edit_blog/".$id);
    } else {
      $this->proses_blog_update();
    }
  }

  private function proses_blog_update()
  {
    $error = FALSE;
    $pesan = "";
    $input = (object)$this->input->post();
    $this->db->trans_begin();

    $data = [
      "kategori_id" => html_escape($this->db->escape_str($input->kategori_id)),
      "publish_id" => html_escape($this->db->escape_str($input->publish_id)),
      "judul" => html_escape($this->db->escape_str($input->judul)),
      "tanggal_update" => date("Y-m-d"),
      "article_pendek" => $input->article_pendek,
      "article" => $input->article
    ];

    $check = $this->M_data->updateData($data, ["blog_id" => $input->blog_id], "blog");
    if ($check) {
      $gambar = $_FILES["gambar"]["name"];
      if ($gambar != "") {
        $gambar_sebelumnya = $this->M_data->editData(["blog_id" => $input->blog_id], "blog")->row();

        $config['upload_path']          = './assets/img/blog/';
        $config['allowed_types']        = 'gif|jpg|png|jpeg';
        $config['max_size']             = 5048;
        $config['max_width']            = 5024;
        $config['max_height']           = 5024;

        $this->load->library("upload");
        $this->upload->initialize($config);

        if(!$this->upload->do_upload("gambar")) {
          $error = TRUE;
          $pesan = "Gambar gagal Upload!";
        } else {
          unlink("./assets/img/blog/".$gambar_sebelumnya->gambar);
          $data = ["gambar" => $gambar];
          $check2 = $this->M_data->updateData($data, ["blog_id" => $input->blog_id], "blog");
          if (!$check2) {
            $error = TRUE;
            $pesan = "Query2 Failed";
          }
        }
      }
    } else {
      $error = TRUE;
      $pesan = "Query1 Failed";
    }
    if ($error === FALSE) {
      $this->db->trans_commit();
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Artikel berhasil ditambah!')</script>");
      redirect("blog");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', '".$pesan."')</script>");
      redirect("edit_blog/".$input->blog_id);
    }
  }

  public function proses_blog_hapus($data = FALSE)
  {
    $id = (int)$this->db->escape_str($data);
    $check = $this->M_data->editData(["blog_id" => $id], "blog");
    if ($check) {
      unlink("./assets/img/blog/".$check->row()->gambar);
      $check2 = $this->M_data->deleteData(["blog_id" => $id], "blog");
      if ($check2) {
        $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data telah dihapus!')</script>");
        redirect("blog");
      } else {
        $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
        redirect("blog");
      }
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Data tidak ditemukan!')</script>");
      redirect("blog");
    }
  }

  public function pesan()
  {
    $data = [
      "user" => $this->M_data->editData(["user_id" => $this->session->userdata("admin_id")],"user")->row(),
      "title" => "Pesan",
      "pesan" => $this->M_data->getData("pesan")->result()
    ];
      $this->load->view("templates/header_admin", $data);
      $this->load->view("templates/sidebar_admin", $data);
      $this->load->view("admin/v_pesan", $data);
      $this->load->view("templates/footer_admin");
  }

  public function pesan_hapus($pesan_id)
  {
    $id = (int)$pesan_id;
    $check = $this->M_data->deleteData(["pesan_id" => $id], "pesan");
    if ($check) {
      $this->session->set_flashdata('pesan', "<script>sweet('success', 'Sukses!', 'Data berhasil dihapus!')</script>");
      redirect("pesan");
    } else {
      $this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal!', 'Query failed!')</script>");
      redirect("pesan");
    }
  }
}
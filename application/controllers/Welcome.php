<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		// $data = [
		// 	"username" => "sanjaya411",
		// 	"password" => password_hash("anjayy123", PASSWORD_DEFAULT),
		// 	"nama" => "Mohammad Ricky Sanjaya"
		// ];
		// $this->M_data->insertData($data,"user");
	}

	public function index()
	{
		$data = [
			"title" => "My Blog",
			"jumbotron" => $this->M_data->getData("jumbotron")->row(),
			"about_me" => $this->M_data->getData("about_me")->row(),
			"skill" => $this->M_data->getData("skill")->result(),
			"portofolio" => $this->M_data->get_portofolio()->result(),
			"blog" => $this->M_data->get_blog()->result(),
			"sosmed" => $this->M_data->get_sosmed()->result()
		];
		$this->load->view('templates/header', $data);
		$this->load->view('templates/navbar', $data);
		$this->load->view('index', $data);
		$this->load->view('templates/footer');
	}

	public function blog_detail($id)
	{
		$blog_id = (int)$id;
		$check = $this->M_data->get_blog_detail($id);
		if ($check) {
			$data = [
				"title" => "Blog",
				"jumbotron" => $this->M_data->getData("jumbotron")->row(),
				"blog" => $check->row()
			];
			$this->load->view('templates/header', $data);
			$this->load->view('templates/navbar', $data);
			$this->load->view('blog_detail', $data);
			$this->load->view('templates/footer');
		} else {
			redirect("page_404");
		}
	}

	public function portofolio_detail($id)
	{
		$portofolio_id = (int)$id;
		$check = $this->M_data->get_portofolio_detail($id);
		if ($check) {
			$data = [
				"title" => "portofolio",
				"jumbotron" => $this->M_data->getData("jumbotron")->row(),
				"portofolio" => $check->row()
			];
			$this->load->view('templates/header', $data);
			$this->load->view('templates/navbar', $data);
			$this->load->view('portofolio_detail', $data);
			$this->load->view('templates/footer');
		} else {
			redirect("page_404");
		}
	}

	public function validation_pesan()
	{
		$this->form_validation->set_rules('nama', 'fieldlabel', 'required');
		$this->form_validation->set_rules('email', 'fieldlabel', 'required|valid_email');
		$this->form_validation->set_rules('subject', 'fieldlabel', 'required');
		$this->form_validation->set_rules('pesan', 'fieldlabel', 'required');
		
		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Isi pesan dengan benar & lengkap!")</script>');
			redirect("index");
		} else {
			$this->proses_pesan_tambah();
		}
	}

	private function proses_pesan_tambah()
	{
		$input = (object)html_escape($this->db->escape_str($this->input->post()));
		$data = [
			"nama" => $input->nama,
			"email" => $input->email,
			"subject" => $input->subject,
			"pesan" => $input->pesan
		];
		$check = $this->M_data->insertData($data, "pesan");
		if ($check) {
			$this->session->set_flashdata('pesan', '<script>sweet("success", "Sukses", "Terima kasih, pesan anda sudah terkirim!")</script>');
			redirect("index");
		} else {
			$this->session->set_flashdata('pesan', '<script>sweet("error", "Gagal", "Query failed!")</script>');
			redirect("index");
		}
	}

	public function page_404()
	{
		$data = [
			"title" => "404 Not Found"
		];
		$this->load->view('templates/header', $data);
		$this->load->view('404_page', $data);
		$this->load->view('templates/footer');
	}

	public function login()
	{
		$this->load->view('v_login');
	}

	public function validation_login()
	{
		$this->form_validation->set_rules('username', 'Username', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() === FALSE) {
			$this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Login!', 'Isi form dengan benar & lengkap!')</script>");
			redirect("login");
		} else {
			$this->proses_login();
			var_dump("SHIT");
		}
	}

	private function proses_login()
	{
		$input = (object)html_escape($this->db->escape_str($this->input->post()));
		$akun = $this->M_data->editData(["username" => $input->username], "user")->row();
		if ($akun) {
			if (password_verify($input->password, $akun->password)) {
				$data = [
					"admin_id" => $akun->user_id,
					"status" => TRUE
				];
				$this->session->set_userdata($data);
				redirect("dashboard");
			} else {
				$this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Login!', 'Password anda salah!')</script>");
				redirect("login");
			}
		} else {
			$this->session->set_flashdata('pesan', "<script>sweet('error', 'Gagal Login!', 'Username tidak ditemukan!')</script>");
			redirect("login");
		}
	}
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
	public $data = array(
		'judul' => 'My CV',
		'nama' => 'SNJY',
		'menu1' => 'Home',
		'menu2' => 'About Me',
		'menu3' => 'Portfolio',
		'menu4' => 'Contact'
	);

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{
		$this->load->view('v_header', $this->data);
		$this->load->view('v_index', $this->data);
		$this->load->view('v_footer', $this->data);
	}

	public function login() {
		
		$this->load->view('v_header', $this->data);
		$this->load->view('v_login');
		$this->load->view('v_footer');
	}
}

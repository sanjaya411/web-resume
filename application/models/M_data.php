<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data extends CI_Model {

  public function getData($table)
  {
    $check = $this->db->get($table);
    if ($check) {
      return $check;
    } else {
      var_dump($this->db->error());
    }
  }

  public function editData($where, $table)
  {
    $check = $this->db->get_where($table, $where);
    if ($check) {
      return $check;
    } else {
      var_dump($this->db->error());
    }
  }

  public function insertData($data, $table)
  {
    $response = (object)[];
    $check = $this->db->insert($table, $data);
    if ($check) {
      $response->success = TRUE;
      $response->message = "Data updated";
    } else {
      $response->message = "Query failed because ".$this->db->error();
    }
    return $response;
  }

  public function updateData($data, $where, $table)
  {
    $response = (object)[];
    $check = $this->db->where($where)->update($table, $data);
    if ($check) {
      $response->success = TRUE;
      $response->message = "Data updated";
    } else {
      $response->message = "Query failed because ".$this->db->error();
    }
    return $response;
  }

  public function deleteData($where, $table)
  {
    $response = (object)[];
    $check = $this->db->delete($table, $where);
    if ($check) {
      $response->success = TRUE;
      $response->message = "Data deleted";
    } else {
      $response->message = "Query failed because ".$this->db->error();
    }
    return $response;
  }

  public function get_portofolio_kategori()
  {
    return $this->db->select("*")
                    ->from("`portofolio` p")
                    ->join("`kategori` k", "p.`kategori_id` = k.`kategori_id`")
                    ->join("`publish` pb", "pb.`publish_id` = p.`publish_id`")
                    ->get();
  }

  public function get_portofolio()
  {
    return $this->db->select("*")
                    ->from("`portofolio` p")
                    ->join("`kategori` k", "p.`kategori_id` = k.`kategori_id`")
                    ->join("`publish` pb", "pb.`publish_id` = p.`publish_id`")
                    ->where("p.`publish_id`", 1)
                    ->get();
  }

  public function get_blog_kategori()
  {
    return $this->db->select("*")
                    ->from("`blog` b")
                    ->join("`kategori` k", "b.`kategori_id` = k.`kategori_id`")
                    ->join("`user` u", "u.`user_id` = b.`penulis`")
                    ->join("`publish` pb", "pb.`publish_id` = b.`publish_id`")
                    ->get();
  }

  public function get_blog()
  {
    return $this->db->select("*")
                    ->from("`blog` b")
                    ->join("`kategori` k", "b.`kategori_id` = k.`kategori_id`")
                    ->join("`user` u", "u.`user_id` = b.`penulis`")
                    ->join("`publish` pb", "pb.`publish_id` = b.`publish_id`")
                    ->where("b.`publish_id`", 1)
                    ->limit(3)
                    ->get();
  }

  public function get_sosmed_list()
  {
    return $this->db->select("*")
                    ->from("`sosmed` s")
                    ->join("`publish` p", "s.`publish_id` = p.`publish_id`")
                    ->get();
  }

  public function get_sosmed()
  {
    return $this->db->select("*")
                    ->from("`sosmed` s")
                    ->join("`publish` p", "s.`publish_id` = p.`publish_id`")
                    ->where("s.`publish_id`", 1)
                    ->get();
  }

  public function get_blog_detail($id)
  {
    return $this->db->select("*")
                    ->from("`blog` b")
                    ->join("`user` u", "b.`penulis` = u.`user_id`")
                    ->join("`kategori` k", "k.`kategori_id` = b.`kategori_id`")
                    ->where("b.`blog_id`", $id)
                    ->get();
  }

  public function get_portofolio_detail($id)
  {
    return $this->db->select("*")
                    ->from("`portofolio` p")
                    ->join("`kategori` k", "p.`kategori_id` = k.`kategori_id`")
                    ->join("`publish` pb", "pb.`publish_id` = p.`publish_id`")
                    ->where("p.`portofolio_id`", $id)
                    ->get();
  }
}
<?php echo $this->session->flashdata('pesan');
$this->session->sess_destroy(); ?>
  <!-- ======= Intro Section ======= -->
  <div id="home" class="intro route bg-image" style="background-image: url(./assets/img/foto/<?= $jumbotron->background_gambar ?>)">
    <div class="overlay-itro"></div>
    <div class="intro-content display-table">
      <div class="table-cell">
        <div class="container">
          <!--<p class="display-6 color-d">Hello, world!</p>-->
          <h1 class="intro-title mb-4"><?= $jumbotron->title_jumbotron; ?></h1>
          <p class="intro-subtitle"><span class="text-slider-items"><?= $jumbotron->text_jumbotron; ?></span><strong class="text-slider"></strong></p>
          <!-- <p class="pt-3"><a class="btn btn-primary btn js-scroll px-4" href="#about" role="button">Learn More</a></p> -->
        </div>
      </div>
    </div>
  </div><!-- End Intro Section -->

  <main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="about-mf sect-pt4 route">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="box-shadow-full">
              <div class="row">
                <div class="col-md-6">
                  <div class="row">
                    <div class="col-sm-6 col-md-5">
                      <div class="about-img">
                        <img src="<?= base_url("assets/img/foto/".$about_me->gambar) ?>" class="img-fluid mx-auto d-block rounded b-shadow-a" alt="">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-7">
                      <div class="about-info">
                        <p><span class="title-s">Name: </span> <span><?= $about_me->name; ?></span></p>
                        <p><span class="title-s">Profile: </span> <span><?= $about_me->profile; ?></span></p>
                        <p><span class="title-s">Email: </span> <span><?= $about_me->email; ?></span></p>
                        <p><span class="title-s">Phone: </span> <span><?= $about_me->phone; ?></span></p>
                      </div>
                    </div>
                  </div>
                  <div class="skill-mf">
                    <p class="title-s">Skill</p>
                    <?php foreach($skill as $item) { ?>
                      <span><?= $item->title; ?></span> <span class="pull-right"><?= $item->persentase ?>%</span>
                      <div class="progress">
                        <div class="progress-bar" role="progressbar" style="width: <?= $item->persentase ?>%;" aria-valuenow="<?= $item->persentase ?>" aria-valuemin="0" aria-valuemax="100"></div>
                      </div>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="about-me pt-4 pt-md-0">
                    <div class="title-box-2">
                      <h5 class="title-left">
                        About me
                      </h5>
                    </div>
                    <p class="lead">
                      <?= $about_me->about_me; ?>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End About Section -->

    <!-- ======= Portfolio Section ======= -->
    <section id="work" class="portfolio-mf sect-pt4 route">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="title-box text-center">
              <h3 class="title-a">
                Portfolio
              </h3>
              <p class="subtitle-a">
                Karya sederhana yang pernah dibuat
              </p>
              <div class="line-mf"></div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <?php foreach($portofolio as $item) { ?>
          <div class="col-md-4">
            <div class="work-box">
              <a href="<?= base_url("assets/img/portofolio/".$item->gambar) ?>" data-gall="portfolioGallery" class="venobox">
                <div class="work-img">
                  <img src="<?= base_url("assets/img/portofolio/".$item->gambar) ?>" alt="" class="img-fluid">
                </div>
              </a>
              <div class="work-content">
                <div class="row">
                  <div class="col-sm-8">
                    <h2 class="w-title"><?= $item->judul ?></h2>
                    <div class="w-more">
                      <span class="w-ctegory"><?= $item->kategori ?></span> / <span class="w-date"><?= date("d M Y", strtotime($item->tanggal_buat)); ?></span>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="w-like">
                      <a href="<?= base_url("portofolio_detail/".$item->portofolio_id); ?>"> <span class="ion-ios-plus-outline"></span></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </section><!-- End Portfolio Section -->

    <!-- ======= Blog Section ======= -->
    <section id="blog" class="blog-mf sect-pt4 route">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="title-box text-center">
              <h3 class="title-a">
                Blog
              </h3>
              <p class="subtitle-a">
                Sebuah tulisan sederhana
              </p>
              <div class="line-mf"></div>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <?php foreach($blog as $item) { ?>
          <div class="col-md-4">
            <div class="card card-blog">
              <div class="card-img">
                <a href="<?= base_url("blog_detail/".$item->blog_id) ?>"><img src="<?= base_url("assets/img/blog/".$item->gambar) ?>" alt="" class="img-fluid"></a>
              </div>
              <div class="card-body">
                <div class="card-category-box">
                  <div class="card-category">
                    <h6 class="category"><?= $item->kategori; ?></h6>
                  </div>
                </div>
                <h3 class="card-title"><a href="<?= base_url("blog_detail/".$item->blog_id) ?>"><?= $item->judul ?></a></h3>
                <p class="card-description">
                  <?= $item->article_pendek; ?>
                </p>
              </div>
              <div class="card-footer">
                <div class="post-author">
                  <a href="#">
                    <img src="<?= base_url("assets/img/foto/".$item->foto) ?>" alt="" class="avatar rounded-circle">
                    <span class="author"><?= $item->nama; ?></span>
                  </a>
                </div>
                <div class="post-date">
                  <span class="ion-ios-clock-outline"></span> <?= date("d M Y", strtotime($item->tanggal_buat)); ?>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </section><!-- End Blog Section -->

    <!-- ======= Contact Section ======= -->
    <section class="paralax-mf footer-paralax bg-image sect-mt4 route" style="background-image: url(assets/img/overlay-bg.jpg)">
      <div class="overlay-mf"></div>
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
            <div class="contact-mf">
              <div id="contact" class="box-shadow-full">
                <div class="row">
                  <div class="col-md-6">
                    <div class="title-box-2">
                      <h5 class="title-left">
                        Kirim pesan
                      </h5>
                    </div>
                    <div>
                      <form action="<?= base_url("validation_pesan"); ?>" method="post">
                        <div class="row">
                          <div class="col-md-12 mb-3">
                            <div class="form-group">
                              <input type="text" name="nama" class="form-control" placeholder="Your Name" />
                            </div>
                          </div>
                          <div class="col-md-12 mb-3">
                            <div class="form-group">
                              <input type="email" class="form-control" name="email" placeholder="Your Email"/>
                            </div>
                          </div>
                          <div class="col-md-12 mb-3">
                            <div class="form-group">
                              <input type="text" class="form-control" name="subject" placeholder="Subject" />
                            </div>
                          </div>
                          <div class="col-md-12">
                            <div class="form-group">
                              <textarea class="form-control" name="pesan" rows="5" placeholder="Message"></textarea>
                            </div>
                          </div>
                          <div class="col-md-12 text-center">
                            <button type="submit" class="button button-a button-big button-rouded">Send Message</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="title-box-2 pt-4 pt-md-0">
                      <h5 class="title-left">
                        Hubungi Saya
                      </h5>
                    </div>
                    <div class="more-info">
                      <p class="lead">
                        <?= $about_me->text_contact; ?>
                      </p>
                      <ul class="list-ico">
                        <li><span class="ion-ios-location"></span><?= $about_me->alamat; ?></li>
                        <li><span class="ion-ios-telephone"></span><?= $about_me->phone; ?></li>
                        <li><span class="ion-email"></span><?= $about_me->email; ?></li>
                      </ul>
                    </div>
                    <div class="socials">
                      <ul>
                        <?php foreach($sosmed as $item) { ?>
                        <li><a href="<?= $item->link; ?>" target="_blank"><span class="ico-circle"><i class="<?= $item->ikon; ?>"></i></span></a></li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->
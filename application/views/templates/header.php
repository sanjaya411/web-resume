<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title><?= $title; ?></title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url("assets/"); ?>img/favicon.png" rel="icon">
  <link href="<?= base_url("assets/"); ?> img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url("assets/") ?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url("assets/") ?>vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?= base_url("assets/") ?>vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="<?= base_url("assets/") ?>vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="<?= base_url("assets/") ?>vendor/venobox/venobox.css" rel="stylesheet">

  <!-- Sweet Alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!-- Custom JS -->
  <script src="http://localhost/CI/Profile/assets/js/custom.js"></script>

  <!-- Template Main CSS File -->
  <link href="<?= base_url("assets/") ?>css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: DevFolio - v2.3.0
  * Template URL: https://bootstrapmade.com/devfolio-bootstrap-portfolio-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body id="page-top">
  <!-- ======= Footer ======= -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="copyright-box">
            <p class="copyright">&copy; Copyright <strong>SanjayaDev</strong>. All Rights Reserved</p>
          </div>
        </div>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <div id="preloader"></div>

  <!-- Vendor JS Files -->
  <script src="<?= base_url("assets/") ?>vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/php-email-form/validate.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/counterup/jquery.counterup.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/typed.js/typed.min.js"></script>
  <script src="<?= base_url("assets/") ?>vendor/venobox/venobox.min.js"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url("assets/") ?>js/main.js"></script>

</body>

</html>
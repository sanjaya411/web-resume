 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="<?= base_url("assets/") ?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Dashboard</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?= base_url("assets/img/foto/".$user->foto); ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?= $user->nama; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="<?= base_url("dashboard"); ?>" class="nav-link <?= $title == 'Dashboard' ? 'active' : false; ?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>Dashboard</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("website_settings"); ?>" class="nav-link <?= $title == 'Website Settings' ? 'active' : false; ?>">
              <i class="nav-icon fab fa-chrome"></i>
              <p>Website Settings</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("portofolio"); ?>" class="nav-link <?= $title == 'Portofolio' ? 'active' : false; ?>">
              <i class="nav-icon fas fa-plus-circle"></i>
              <p>Portofolio</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("blog"); ?>" class="nav-link <?= $title == 'Blog' ? 'active' : false; ?>">
              <i class="nav-icon fab fa-blogger"></i>
              <p>Blog</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?= base_url("pesan"); ?>" class="nav-link <?= $title == 'Pesan' ? 'active' : false; ?>">
              <i class="nav-icon fas fa-comment"></i>
              <p>Pesan</p>
            </a>
          </li>
          <li class="nav-item">
            <a href="" data-toggle="modal" data-target="#logout" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>Logout</p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <div class="content-wrapper">
    <section class="content">

<div class="modal fade" id="logout">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <p>Apakah anda yakin?</p>
        <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal">Close</button>
        <a href="<?= base_url("index"); ?>" class="btn btn-danger btn-sm">Logout</a>
      </div>
    </div>
  </div>
</div>
<div class="intro intro-single route bg-image" style="background-image: url(assets/img/overlay-bg.jpg)">
  <div class="overlay-mf"></div>
  <div class="intro-content display-table">
    <div class="table-cell">
      <div class="container">
        <h4 class="intro-title mb-4">Portfolio Details</h4>
        <ol class="breadcrumb d-flex justify-content-center">
          <li class="breadcrumb-item">
            <a href="#">Home</a>
          </li>
          <li class="breadcrumb-item active">Portfolio Details</li>
        </ol>
      </div>
    </div>
  </div>
</div>

  <main id="main">

    <!-- ======= Portfolio Details Section ======= -->
    <section class="portfolio-details">
      <div class="container">

        <div class="portfolio-details-container">

          <div class="owl-carousel portfolio-details-carousel">
            <img src="<?= base_url("assets/img/portofolio/".$portofolio->gambar) ?>" class="img-fluid" alt="">
          </div>

          <div class="portfolio-info">
            <h3>Project information</h3>
            <ul>
              <li><strong>Category</strong>: <?= $portofolio->kategori ?></li>
              <li><strong>Project date</strong>: <?= date("d M Y", strtotime($portofolio->tanggal_buat)); ?></li>
            </ul>
          </div>

        </div>

        <div class="portfolio-description">
          <h2><?= $portofolio->judul ?></h2>
          <?= $portofolio->keterangan; ?>
        </div>
      </div>
    </section><!-- End Portfolio Details Section -->

  </main><!-- End #main -->
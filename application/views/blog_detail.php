<div class="intro intro-single route bg-image" style="background-image: url(assets/img/overlay-bg.jpg)">
  <div class="overlay-mf"></div>
  <div class="intro-content display-table">
    <div class="table-cell">
      <div class="container">
        <h2 class="intro-title mb-4">Blog Details</h2>
      </div>
    </div>
  </div>
</div>

  <main id="main">

    <!-- ======= Blog Single Section ======= -->
    <section class="blog-wrapper sect-pt4" id="blog">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="post-box">
              <div class="post-thumb">
                <img src="<?= base_url("assets/img/blog/".$blog->gambar); ?>" class="img-fluid" alt="">
              </div>
              <div class="post-meta">
                <h1 class="article-title"><?= $blog->judul; ?></h1>
                <ul>
                  <li>
                    <span class="ion-ios-person"></span>
                    <a href="#"><?= $blog->nama; ?></a>
                  </li>
                  <li>
                    <span class="ion-pricetag"></span>
                    <a href="#"><?= $blog->kategori ?></a>
                  </li>
                </ul>
              </div>
              <div class="article-content">
                <?= $blog->article; ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Blog Single Section -->

  </main><!-- End #main -->
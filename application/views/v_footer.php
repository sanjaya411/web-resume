 
  <!-- jQuery JS File -->
  <script src="<?php echo base_url()?>asset/js/jquery.js"></script>

  <!-- Bootstrap JS File -->
  <script src="<?php echo base_url()?>asset/js/bootstrap.min.js"></script>

  <!-- Font Awesome -->
  <script src="https://kit.fontawesome.com/a076d05399.js"></script>

  <!-- Swiper JS -->
  <script src="<?php echo base_url()?>asset/js/swiper.js"></script>
  <script>
    var swiper = new Swiper('.swiper-container', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
      autoplay: {
				delay: 2500,
				disableOnInteraction: false,
			},
    });
  </script>
</body>
</html>
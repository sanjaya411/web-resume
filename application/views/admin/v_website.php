<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="py-2">Website Settings</h4>
  <div class="card">
    <div class="card-body">
      <ul class="nav nav-pills">
        <li class="nav-item">
          <a class="nav-link active" data-toggle="pill" href="#jumbotron">Jumbotron</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#menu1">About Me</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#menu2">Skill</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#menu3">Sosmed</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="pill" href="#menu4">Kategori</a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane container active" id="jumbotron">
          <?= form_open_multipart("validation_jumbotron", "class='my-3'") ?>
          <div class="form-group">
            <?= form_label("Title"); ?>
            <?= form_input("title", $jumbotron->title, "class='form-control' required") ?>
          </div>
          <div class="form-group">
            <?= form_label("Background Gambar"); ?>
            <?= form_upload("background_gambar", "", "class='form-control'") ?>
          </div>
          <div class="form-group">
            <?= form_label("Title Jumbotron"); ?>
            <?= form_input("title_jumbotron", $jumbotron->title_jumbotron, "class='form-control' required") ?>
          </div>
          <div class="form-group">
            <?= form_label("Text Jumbotron"); ?>
            <?= form_input("text_jumbotron", $jumbotron->text_jumbotron, "class='form-control' required"); ?>
          </div>
          <?= form_submit("Submit", "Submit", "class='btn btn-success btn-sm'") ?>
          <?= form_close(); ?>
        </div>
        <div class="tab-pane container fade" id="menu1">
          <?= form_open_multipart("validation_about_me", "class='my-3'"); ?>
            <div class="form-group">
              <?= form_label("Nama"); ?>
              <?= form_input("name", $about_me->name, "class='form-control' required"); ?>
            </div>
            <img src="<?= base_url("assets/img/foto/".$about_me->gambar); ?>" alt="Gambar saya" style="max-width: 15%;">
            <div class="form-group">
              <?= form_label("Foto"); ?>
              <?= form_upload("gambar", "", "class='form-control'"); ?>
            </div>
            <div class="form-group">
              <?= form_label("Profile"); ?>
              <?= form_input("profile", $about_me->profile, "class='form-control' required"); ?>
            </div>
            <div class="form-group">
              <?= form_label("email"); ?>
              <?= form_input("email", $about_me->email, "class='form-control' required"); ?>
            </div>
            <div class="form-group">
              <?= form_label("No Hp"); ?>
              <?= form_input("phone", $about_me->phone, "class='form-control' required"); ?>
            </div>
            <div class="form-group">
              <?= form_label("Alamat"); ?>
              <?= form_input("alamat", $about_me->alamat, "class='form-control' required"); ?>
            </div>
            <div class="form-group">
              <?= form_label("About Me"); ?>
              <?= form_textarea("about_me", $about_me->about_me, "class='form-control' id='editor1'") ?>
            </div>
            <div class="form-group">
              <?= form_label("Text Contact"); ?>
              <?= form_textarea("text_contact", $about_me->text_contact, "class='form-control' id='editor2'") ?>
            </div>
            <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'") ?>
          <?= form_close(); ?>
        </div>
        <div class="tab-pane container fade" id="menu2">
          <button class="btn btn-primary btn-sm my-3" data-toggle="modal" data-target="#skillAdd">Tambah Skill</button>
          <table class="table table-bordered" id="data">
            <thead>
              <tr>
                <th>Nomor</th>
                <th>Title</th>
                <th>Persentase</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1; foreach($skill as $item) { ?>
              <tr>
                <td><?= $no++; ?></td>
                <td><?= $item->title; ?></td>
                <td><?= $item->persentase; ?></td>
                <td>
                  <a href="<?= base_url("view_skill_edit/".$item->skill_id); ?>" class="btn btn-info btn-sm">Edit</a>
                  <a href="<?= base_url("proses_skill_hapus/".$item->skill_id) ?>" class="btn btn-danger btn-sm">Hapus</a>
                </td>
              </tr>  
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="tab-pane container fade" id="menu3">
          <button class="btn btn-success btn-sm my-3" data-toggle="modal" data-target="#addSosmed">Tambah Sosmed</button>
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="data2">
              <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Sosmed</th>
                  <th>Icon</th>
                  <th>Link</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no = 1; foreach($sosmed as $item) { ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?= $item->nama; ?></td>
                  <td><?= $item->ikon; ?></td>
                  <td><?= $item->link; ?></td>
                  <td><?= $item->publish; ?></td>
                  <td>
                    <a href="<?= base_url("sosmed_edit/".$item->sosmed_id); ?>" class="btn btn-info btn-sm">Edit</a>
                    <a href="<?= base_url("sosmed_hapus/".$item->sosmed_id) ?>" class="btn btn-danger btn-sm">Hapus</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="tab-pane container fade" id="menu4">
          <button class="my-3 btn btn-success btn-sm" data-toggle="modal" data-target="#addKategori">Tambah Kategori</button>
          <div class="table-responsive">
            <table class="table table-bordered" id="data3">
              <thead>
                <tr>
                  <th>Nomor</th>
                  <th>Kategori</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php $no=1; foreach($kategori as $item) { ?>
                <tr>
                  <td><?= $no++; ?></td>
                  <td><?= $item->kategori; ?></td>
                  <td>
                    <a href="<?= base_url("proses_kategori_hapus/".$item->kategori_id) ?>" class="btn btn-danger btn-sm">Hapus</a>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addKategori">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Kategori</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("proses_kategori_tambah"); ?>
          <div class="form-group">
            <?= form_label("Kategori"); ?>
            <?= form_input("kategori", "", "class='form-control' required"); ?>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="skillAdd">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Skill</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("validation_skill"); ?>
          <div class="form-group">
            <?= form_label("Skill"); ?>
            <?= form_input("title", "", "class='form-control' id='skill'") ?>
          </div>
          <div class="form-group">
            <?= form_label("Persentase"); ?>
            <?= form_input("persentase", "", "class='form-control' id='persentase'"); ?>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'") ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addSosmed">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5>Tambah Sosmed</h5>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open("validation_sosmed"); ?>
          <div class="form-group">
            <?= form_label("Nama Sosmed"); ?>
            <?= form_input("nama", "", "class='form-control' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Ikon Sosmed"); ?>
            <?= form_input("ikon", "", "class='form-control' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Link Sosmed"); ?>
            <?= form_input("link", "", "class='form-control' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Status Publish"); ?>
            <select name="publish_id" class="form-control" required>
              <option disabled selected>-- Pilih Status ---</option>
              <?php foreach($publish as $item) { ?>
              <option value="<?= $item->publish_id; ?>"><?= $item->publish; ?></option>
              <?php } ?>
            </select>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
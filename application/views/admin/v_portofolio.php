<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="py-2">Portofolio</h4>
  <div class="card">
    <div class="card-body">
      <button class="mb-3 btn btn-success btn-sm" data-toggle="modal" data-target="#addPortofolio">Tambah Portofolio</button>
      <div class="table-responsive">
        <table class="table table-bordered" id="data">
          <thead>
            <tr>
              <th>Nomor</th>
              <th>Judul</th>
              <th>Gambar</th>
              <th>Kategori</th>
              <th>Tanggal Buat</th>
              <th>Tanggal Upload</th>
              <th>Tanggal Update</th>
              <th>Keterangan</th>
              <th>Publish</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($portofolio as $item) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $item->judul; ?></td>
              <td><img src="<?= base_url("assets/img/portofolio/".$item->gambar); ?>" style="max-width: 70px;"></td>
              <td><?= $item->kategori; ?></td>
              <td><?= date("d M Y", strtotime($item->tanggal_buat)); ?></td>
              <td><?= date("d M Y", strtotime($item->tanggal_upload)); ?></td>
              <td><?= date("d M Y", strtotime($item->tanggal_update)); ?></td>
              <td><?= $item->keterangan; ?></td>
              <td><?= $item->publish; ?></td>
              <td>
                <a href="<?= base_url("edit_portofolio/".$item->portofolio_id); ?>" class="btn btn-info btn-sm">Edit</a>
                <a href="<?= base_url("hapus_portofolio/".$item->portofolio_id) ?>" class="btn btn-danger btn-sm">Hapus</a>
              </td>
            </tr>  
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="addPortofolio">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Portofolio</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open_multipart("validation_portofolio_tambah"); ?>
          <div class="form-group">
            <?= form_label("Judul"); ?>
            <?= form_input("judul", "", "class='form-control' required") ?>
          </div>
          <div class="form-group">
            <?= form_label("Gambar"); ?>
            <?= form_upload("gambar", "", "class='form-control' required"); ?>
          </div>
          <div class="form-grop">
            <?= form_label("Kategori"); ?>
            <select name="kategori_id" class="form-control" required>
              <option selected disabled>-- Pilih Kategori --</option>
              <?php foreach($kategori as $item) { ?>
              <option value="<?=$item->kategori_id ?>"><?= $item->kategori; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <?= form_label("Tanggal Buat"); ?>
            <input type="date" name="tanggal_buat" class="form-control">
          </div>
          <div class="form-group">
            <?= form_label("Keterangan"); ?>
            <?= form_textarea("keterangan", "", "class='form-control' id='editor1' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Publish"); ?>
            <select name="publish_id" class="form-control" required>
              <option disabled selected>-- Pilih Publish --</option>
              <option value="1">Publish</option>
              <option value="2">Not Publish</option>
              <option value="3">Archived</option>
            </select>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'") ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
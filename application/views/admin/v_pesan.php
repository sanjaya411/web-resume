<?php echo $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="my-3">Pesan</h4>
  <div class="card">
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="data">
          <thead>
            <tr>
              <th>Nomor</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Subject</th>
              <th>Pesan</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($pesan as $item) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $item->nama; ?></td>
              <td><?= $item->email; ?></td>
              <td><?= $item->subject; ?></td>
              <td><?= $item->pesan; ?></td>
              <td>
                <a href="<?= base_url("pesan_hapus/".$item->pesan_id); ?>" class="btn btn-danger btn-sm">Hapus</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
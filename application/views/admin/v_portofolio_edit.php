<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="py-2">Edit Portofolio</h4>
  <div class="card">
    <div class="card-body">
      <?= form_open_multipart("validation_portofolio_edit"); ?>
          <div class="form-group">
            <?= form_label("Judul"); ?>
            <?= form_hidden("portofolio_id", $portofolio->portofolio_id) ?>
            <?= form_input("judul", $portofolio->judul, "class='form-control' required") ?>
          </div>
          <img src="<?= base_url("assets/img/portofolio/".$portofolio->gambar) ?>" style="max-width: 30%;">
          <div class="form-group">
            <?= form_label("Gambar"); ?>
            <?= form_upload("gambar", "", "class='form-control'"); ?>
          </div>
          <div class="form-grop">
            <?= form_label("Kategori"); ?>
            <select name="kategori_id" class="form-control" required>
              <option selected disabled>-- Pilih Kategori --</option>
              <?php foreach($kategori as $item) { ?>
              <option <?php if($portofolio->kategori_id == $item->kategori_id) { echo "selected"; } ?> value="<?=$item->kategori_id ?>"><?= $item->kategori; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <?= form_label("Tanggal Buat"); ?>
            <input type="date" name="tanggal_buat" class="form-control" value="<?= $portofolio->tanggal_buat; ?>">
          </div>
          <div class="form-group">
            <?= form_label("Keterangan"); ?>
            <?= form_textarea("keterangan", $portofolio->keterangan, "class='form-control' id='editor1' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Publish"); ?>
            <select name="publish_id" class="form-control" required>
              <option disabled selected>-- Pilih Publish --</option>
              <option <?php if($portofolio->publish_id == 1) { echo "selected"; } ?> value="1">Publish</option>
              <option <?php if($portofolio->publish_id == 2) { echo "selected"; } ?> value="2">Not Publish</option>
              <option <?php if($portofolio->publish_id == 3) { echo "selected"; } ?> value="3">Archived</option>
            </select>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'") ?>
        <?= form_close(); ?>
    </div>
  </div>
</div>
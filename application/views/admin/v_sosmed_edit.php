<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="my-3">Sosmed Edit</h4>
  <div class="card">
    <div class="card-body">
      <?= form_open("validation_sosmed_edit"); ?>
        <div class="form-group">
          <?= form_label("Nama Sosmed"); ?>
          <?= form_hidden("sosmed_id", $sosmed->sosmed_id); ?>
          <?= form_input("nama", $sosmed->nama, "class='form-control' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Ikon Sosmed"); ?>
          <?= form_input("ikon", $sosmed->ikon, "class='form-control' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Link Sosmed"); ?>
          <?= form_input("link", $sosmed->link, "class='form-control' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Status Publish"); ?>
          <select name="publish_id" class="form-control" required>
            <option disabled selected>-- Pilih Status ---</option>
            <?php foreach($publish as $item) { ?>
            <option <?php if($sosmed->publish_id == $item->publish_id) { echo "selected"; } ?> value="<?= $item->publish_id; ?>"><?= $item->publish; ?></option>
            <?php } ?>
          </select>
        </div>
        <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'"); ?>
      <?= form_close(); ?>
    </div>
  </div>
</div>
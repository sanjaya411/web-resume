<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="py-3">Edit Artikel</h4>
  <div class="card">
    <div class="card-body">
    <?= form_open_multipart("validation_blog_edit"); ?>
        <div class="form-group">
          <?= form_label("Judul"); ?>
          <?= form_hidden("blog_id", $blog->blog_id); ?>
          <?= form_input("judul", $blog->judul, "class='form-control' required") ?>
        </div>
        <div class="form-group">
          <?= form_label("Kategori") ?>
          <select name="kategori_id" class="form-control">
            <option selected disabled>-- Pilih Kategori --</option>
            <?php foreach($kategori as $item) { ?>
            <option <?php if($blog->kategori_id == $item->kategori_id) { echo "selected"; } ?> value="<?= $item->kategori_id; ?>"><?= $item->kategori; ?></option>
            <?php } ?>
          </select>
        </div>
        <img src="<?= base_url("assets/img/blog/".$blog->gambar) ?>" style="max-width: 25%;">
        <div class="form-group">
          <?= form_label("Gambar"); ?>
          <?= form_upload("gambar", "", "class='form-control'"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Artikel Pendek"); ?>
          <?= form_textarea("article_pendek", $blog->article_pendek, "class='form-control' id='editor2' required"); ?>
        </div>
        <div class="form-group">
          <?= form_label("Artikel"); ?>
          <?= form_textarea("article", $blog->article, "class='form-control' id='editor1' required") ?>
        </div>
        <div class="form-group">
          <?= form_label("Publish") ?>
          <select name="publish_id" class="form-control">
            <option selected disabled>-- Pilih Publish ---</option>
            <option <?php if($blog->publish_id == 1) { echo "selected"; } ?> value="1">Publish</option>
            <option <?php if($blog->publish_id == 2) { echo "selected"; } ?> value="2">Not Publish</option>
            <option <?php if($blog->publish_id == 3) { echo "selected"; } ?> value="3">Archived</option>
          </select>
        </div>
        <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'"); ?>
      <?= form_close(); ?>
    </div>
  </div>
</div>
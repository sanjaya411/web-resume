<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="py-2">Edit Skill</h4>
  <div class="card">
    <div class="card-body">
      <?= form_open("validation_skill_update"); ?>
          <div class="form-group">
            <?= form_label("Skill"); ?>
            <?= form_hidden("skill_id", $skill->skill_id) ?>
            <?= form_input("title", $skill->title, "class='form-control' id='skill'") ?>
          </div>
          <div class="form-group">
            <?= form_label("Persentase"); ?>
            <?= form_input("persentase", $skill->persentase, "class='form-control' id='persentase'"); ?>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'") ?>
        <?= form_close(); ?>
    </div>
  </div>
</div>
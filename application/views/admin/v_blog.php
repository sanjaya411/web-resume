<?= $this->session->flashdata('pesan'); ?>
<div class="container-fluid">
  <h4 class="py-3">Blog</h4>
  <div class="card">
    <div class="card-body">
      <button class="btn btn-success btn-sm mb-3" data-toggle="modal" data-target="#addBlog">Tambah Artikel</button>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="data">
          <thead>
            <tr>
              <th>Nomor</th>
              <th>Kategori</th>
              <th>Judul</th>
              <th>Tanggal Buat</th>
              <th>Tanggal Update</th>
              <th>Publish</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            <?php $no=1; foreach($blog as $item) { ?>
            <tr>
              <td><?= $no++; ?></td>
              <td><?= $item->kategori; ?></td>
              <td><?= $item->judul; ?></td>
              <td><?= date("d M Y", strtotime($item->tanggal_buat)); ?></td>
              <td><?= date("d M Y", strtotime($item->tanggal_update)); ?></td>
              <td><?= $item->publish; ?></td>
              <td>
                <a href="<?= base_url("edit_blog/".$item->blog_id) ?>" class="btn btn-info btn-sm">Edit</a>
                <a href="<?= base_url("hapus_blog/".$item->blog_id) ?>" class="btn btn-danger btn-sm">Hapus</a>
              </td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div class="modal fade" id="addBlog">
  <div class="modal-dialog modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Blog</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <?= form_open_multipart("validation_blog"); ?>
          <div class="form-group">
            <?= form_label("Judul"); ?>
            <?= form_input("judul", "", "class='form-control' required") ?>
          </div>
          <div class="form-group">
            <?= form_label("Kategori") ?>
            <select name="kategori_id" class="form-control">
              <option selected disabled>-- Pilih Kategori --</option>
              <?php foreach($kategori as $item) { ?>
              <option value="<?= $item->kategori_id; ?>"><?= $item->kategori; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <?= form_label("Gambar"); ?>
            <?= form_upload("gambar", "", "class='form-control' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Artikel Pendek"); ?>
            <?= form_textarea("article_pendek", "", "class='form-control' id='editor2' required"); ?>
          </div>
          <div class="form-group">
            <?= form_label("Artikel"); ?>
            <?= form_textarea("article", "", "class='form-control' id='editor1' required") ?>
          </div>
          <div class="form-group">
            <?= form_label("Publish") ?>
            <select name="publish_id" class="form-control">
              <option selected disabled>-- Pilih Publish ---</option>
              <option value="1">Publish</option>
              <option value="2">Not Publish</option>
              <option value="3">Archived</option>
            </select>
          </div>
          <?= form_submit("submit", "Submit", "class='btn btn-success btn-sm'"); ?>
        <?= form_close(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="<?php echo base_url()?>asset/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>asset/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url()?>asset/css/swiper.css">

  <title><?= $judul; ?></title>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="10">
  <nav class="navbar navbar-expand-md navbar-dark bg-primary fixed-top">
    <div class="container">
      <a href="#" class="navbar-brand">SNJY</a>
      <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#Navbar">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="Navbar">
        <ul class="navbar-nav">
          <a href="#home" class="nav-item nav-link active"><?= $menu1;?></a>
          <a href="#tentang" class="nav-item nav-link"><?= $menu2;?></a>
          <a href="#project" class="nav-item nav-link"><?= $menu3;?></a>
          <a href="#contact" class="nav-item nav-link"><?= $menu4;?></a>
        </ul>
        <ul class="navbar-nav ml-auto">
          <a href="<?= base_url().'index.php/page/login'?>" class="btn btn-outline-warning">Login</a>
        </ul>
      </div>
    </div>
  </nav>

<div class="jumbotron bg-primary">
    <div class="container-fluid">
      <div class="row text-center text-white">
        <div class="col-md-6">
          <h1>Halo, saya Sanjaya</h1>
          <h5>Fullstack Web Developer, Network Engineer</h5>
        </div>
        <div class="col-md-6">
          <img src="<?= base_url()?>asset/img/undraw_site_content_ihgn.svg" alt="Illustrator" class="gambar">
        </div>
      </div>
    </div>
  </div>

  <section id="tentang">
    <div class="container-sm shadow">
      <h4 class="text-center mt-4 mb-5">Tentang Saya</h4>
      <div class="row">
        <div class="col-md-5">
          <img src="<?php echo base_url()?>asset/img/ana.jpeg" alt="Saya" class="rounded-circle gambar mx-auto d-block mb-5" width="60%" height="250px">
        </div>
        <div class="col-md-6">
          <div class="card mb-4">
            <div class="card-body">
              <button class="btn btn-primary tanya" data-toggle="collapse" data-target="#pertama">Tentang Saya</button>
              <div class="collapse" id="pertama">
                <p class="mt-3">Halo, nama saya Mohammad Ricky Sanjaya biasa dipanggil Sanjaya. Saya adalah seorang Fullstack Web Developer dan juga Network Engineer. Kesukaan saya terhadap Ilmu IT dimulai sejak masa SMP kelas 8, disitu saya mulai tertarik dengan komputer.</p>
                <p>Pada masa SMK, saya mulai untuk memperdalam ilmu IT sehingga sampai sekarang saya menjadi Fulllsatck Web Developer dan juga Network Engineer. Saya terbiasa membuat website, baik itu dinamis maupun statis, saya juga memahami dan bisa membuat jaringan LAN dan MAN dengan menggunakan Router Mikrotik dan Cisco</p>
                <p>Saya memiliki kemampuan lain seperti Pekerja Keras, Disiplin, Ulet, Teliti, dan bisa berkerja sama dengan Tim. Saya biasa menyelesaikan tugas saya dengan waktu sesingkat mungkin dengan memperhatikan banyak hal sehingga tujuan pekerjaan saya dapat tercapai dengan cepat, efisien dan juga aman.</p>
              </div>
  
              <button class="btn btn-primary tanya mt-3" data-toggle="collapse" data-target="#kedua">Profil</button>
              <div class="collapse" id="kedua">
                <table class="table mt-3">
                  <tr>
                    <th>Nama</th>
                    <td>: Mohammad Ricky Sanjaya</td>
                  </tr>
                  <tr>
                    <th>Tanggal Lahir</th>
                    <td>: 04 Juni 2002</td>
                  </tr>
                  <tr>
                    <th>Status</th>
                    <td>: Single</td>
                  </tr>
                  <tr>
                    <th>HP</th>
                    <td>: 081213490755</td>
                  </tr>
                  <tr>
                    <th>Email</th>
                    <td>: halo@ricky-sanjaya.my.id</td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <section id="project">
    <div class="container-sm" style="margin-top: 100px;">
      <h4 class="text-center">My Project</h4>
      <div class="row">
        <div class="col-md-4">
          <div class="card p-2 mt-3 shadow">
            <img src="<?php echo base_url()?>asset/img/port1.png" width="100%" class="card-img-top">
            <div class="card-body">
              <h5 class="text-center">Sistem Informasi pendataan penduduk</h5>
            </div>
          </div>
        </div>
  
        <div class="col-md-4">
          <div class="card p-2 mt-3 shadow">
            <img src="<?= base_url()?>asset/img/port2.png" width="100%" class="card-img-top">
            <div class="card-body">
              <h5 class="text-center">Landing page eskul Olpres</h5>
            </div>
          </div>
        </div>
  
        <div class="col-md-4">
          <div class="card p-2 mt-3 shadow">
            <img src="<?= base_url()?>asset/img/port3.png" width="100%" class="card-img-top">
            <div class="card-body">
              <h5 class="text-center">Sistem Informasi Laundry</h5>
            </div>
          </div>
        </div>
      </div>
  
      <div class="row">
        <div class="col-md-4">
          <div class="card p-2 mt-3 shadow">
            <img src="<?= base_url()?>asset/img/port4.jpeg" width="100%" class="card-img-top">
            <div class="card-body">
              <h5 class="text-center">Sistem Informasi Kelas</h5>
            </div>
          </div>
        </div>
  
        <div class="col-md-4">
          <div class="card p-2 mt-3 shadow">
            <img src="<?= base_url()?>asset/img/cisco.png" width="100%" class="card-img-top">
            <div class="card-body">
              <h5 class="text-center">LAN dengan VLAN dan VLSM</h5>
            </div>
          </div>
        </div>
  
        <div class="col-md-4">
          <div class="card p-2 mt-3 shadow">
            <img src="<?= base_url()?>asset/img/cisco2.jfif" width="100%" class="card-img-top">
            <div class="card-body">
              <h5 class="text-center">MAN dengan VLSM dan EIGRP</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <section class="bg-primary p-3" style="margin-top: 100px;">
    <h4 class="text-center text-white mb-3">Certificate</h4>
      <div class="swiper-container mb-5">
        <div class="swiper-wrapper">
          <div class="swiper-slide">
            <img src="<?= base_url()?>asset/img/cer1.png" alt="" class="swip">
          </div>
          <div class="swiper-slide">
            <img src="<?= base_url()?>asset/img/cer2.jpeg" alt="" class="swip">
          </div>
          <div class="swiper-slide">
            <img src="<?= base_url()?>asset/img/cer3.jpeg" alt="" class="swip">
          </div>
          <div class="swiper-slide">
            <img src="<?= base_url()?>asset/img/gunnebo.jpg" alt="" class="swip">
          </div>
          <div class="swiper-slide">
            <img src="<?= base_url()?>asset/img/ubuntu.jpg" alt="" class="swip">
          </div>
          <div class="swiper-slide">
            <img src="<?= base_url()?>asset/img/sekolah.jpg" alt="" class="swip">
          </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
      </div>
  </section>

  <section id="contact">
    <div class="row mt-5 mb-5">
      <div class="col-10 mx-auto">
        <h4 class=" text-center mb-4">Kontak</h4>
        <div class="row">
          <div class="col-md-6 mt-3">
            <div class="card shadow">
              <div class="card-body">
                <h5 class=" text-center">Kontak Saya</h5>
                  <p>Halo teman-teman, jika teman-teman ada yang ingin ditanyakan mengenai teknologi, sharing ataupun Bisnis, kalian bisa menghubungi saya melalui kontak dibawah ini</p>
                  <a href="https://api.whatsapp.com/send?phone=6281213490755" target="_blank" class="text-black-50"><i class="fab fa-whatsapp fa-2x mr-2"></i>+62 812-1349-0755</a> <br>
                  <a href="https://t.me/m_ricky_sanjaya" target="_blank" class="text-black-50"><i class="fab fa-telegram fa-2x mr-2 mt-2"></i>+62 812-1349-0755</a> <br>
                  <a href="github.com/SanjayaDev" target="_blank" class="text-black-50"><i class="fab fa-github fa-2x mr-2 mt-2"></i>SanjayaDev</a> <br>
                  <a href="https://www.instagram.com/sanjaya_411/" target="_blank" class="text-black-50"><i class="fab fa-instagram fa-2x mr-2 mt-2"></i>@sanjaya_411</a>
              </div>
            </div>
          </div>
          <div class="col-md-6 mt-3">
            <div class="card shadow">
              <div class="card-body">
                <h5 class=" text-center">Atau teman-teman bisa mengisi form dibawah</h5>
                <div class="row mt-4">
                  <div class="col-10 mx-auto">
                    <form action="" method="post">
                        <div class="form-group">
                            <label>Nama Lengkap</label>
                            <input type="text" name="nama" class=" form-control">
                          </div>
      
                          <div class="form-group">
                            <label>Email</label>
                            <input type="email" name="email" class=" form-control">
                          </div>
      
                          <div class="form-group">
                            <label>No. HP</label>
                            <input type="number" name="hp" class=" form-control">
                          </div>
      
                          <div class="form-group">
                            <label>Pesan</label>
                            <textarea name="pesan" class=" form-control" cols="30" rows="5"></textarea>
                          </div>

                          <input type="submit" value="Kirim" class="btn btn-primary btn-md">
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <footer class="bg-primary p-3 text-white text-center">
    <p>Copyright &copy; ricky-sanjaya.my.id, 2020</p>
  </footer>
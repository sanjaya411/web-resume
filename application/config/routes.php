<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = 'welcome/page_404';
$route['translate_uri_dashes'] = FALSE;

$route["index"] = "welcome/index";
$route["login"] = "welcome/login";
$route["validation_login"] = "welcome/validation_login";
$route["proses_login"] = "welcome/proses_login";
$route["blog_detail/(.+)"] = "welcome/blog_detail/$1";
$route["portofolio_detail/(.+)"] = "welcome/portofolio_detail/$1";
$route["validation_pesan"] = "welcome/validation_pesan";

$route["dashboard"] = "admin/index";
$route["website_settings"] = "admin/website_settings";
$route["validation_jumbotron"] = "admin/validation_jumbotron";
$route["validation_about_me"] = "admin/validation_about_me";
$route["view_skill_edit/(.+)"] = "admin/view_skill_edit/$1";
$route["validation_skill"] = "admin/validation_skill";
$route["validation_skill_update"] = "admin/validation_skill_update";
$route["proses_skill_hapus/(.+)"] = "admin/proses_skill_hapus/$1";
$route["validation_sosmed"] = "admin/validation_sosmed";
$route["sosmed_edit/(.+)"] = "admin/sosmed_edit/$1";
$route["validation_sosmed_edit"] = "admin/validation_sosmed_edit";
$route["sosmed_hapus/(.+)"] = "admin/sosmed_hapus/$1";

$route["portofolio"] = "admin/portofolio";
$route["validation_portofolio_tambah"] = "admin/validation_portofolio_tambah";
$route["edit_portofolio/(.+)"] = "admin/view_portofolio_edit/$1";
$route["validation_portofolio_edit"] = "admin/validation_portofolio_edit";
$route["hapus_portofolio/(.+)"] = "admin/proses_portofolio_hapus/$1";

$route["blog"] = "admin/view_blog";
$route["validation_blog"] = "admin/validation_blog";
$route["edit_blog/(.+)"] = "admin/view_blog_edit/$1";
$route["validation_blog_edit"] = "admin/validation_blog_edit";
$route["hapus_blog/(.+)"] = "admin/proses_blog_hapus/$1";

$route["pesan"] = "admin/pesan";
$route["pesan_hapus/(.+)"] = "admin/pesan_hapus/$1";

$route["proses_kategori_tambah"] = "admin/proses_kategori_tambah";
$route["proses_kategori_hapus/(.+)"] = "admin/proses_kategori_hapus/$1";